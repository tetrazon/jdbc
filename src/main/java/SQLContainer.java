import java.sql.SQLException;
import java.util.List;

public class SQLContainer implements DBController {
    private static DBController dbController;
    public static void  registerController(String className) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class dBToOperateController = Class.forName(className);
        Object obj = dBToOperateController.newInstance();
        dbController = (DBController) obj;
    }

    @Override
    public void addUser(User user) throws SQLException {
        dbController.addUser(user);
    }

    @Override
    public void deleteUserByName(String name) throws SQLException {
        dbController.deleteUserByName(name);
    }

    @Override
    public List<User> getAllUsers() throws SQLException {
        return dbController.getAllUsers();
    }

    @Override
    public void deleteAllUsers() throws SQLException {
        dbController.deleteAllUsers();
    }
}
