import javax.naming.ldap.Control;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySQLController implements DBController {
    private String username;
    private String password;
    private String URL;

    public MySQLController() {
        username = "root";
        password = "root";
        URL = "jdbc:mysql://localhost:3306/test?verifyServerCertificate=false&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    }

    public void addUser(User user) throws SQLException {
        int age = user.getAge();
        String name = user.getName();
        String mail = user.getMail();
        try (Connection connection = DriverManager.getConnection(URL, username, password); PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO user_info (name, age, mail)" + " VALUES (?, ?, ?);");) {
            preparedStatement.setString(1, name);
            preparedStatement.setInt(2, age);
            preparedStatement.setString(3, mail);
            preparedStatement.executeUpdate();
            System.out.println("added user: " + name + ", age" + age + ", mail " + mail);
        } catch (SQLIntegrityConstraintViolationException e) {
            System.out.println("user with that mail already exists");
        }
    }

    public void deleteUserByName(String name) throws SQLException {
        try (Connection connection = DriverManager.getConnection(URL, username, password)) {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM user_info WHERE NAME = ?");
            preparedStatement.setString(1, name);
            preparedStatement.executeUpdate();
            System.out.println("deleted user: " + name);
        }
    }

    @Override
    public void deleteAllUsers() throws SQLException {
        try (Connection connection = DriverManager.getConnection(URL, username, password); Statement statement = connection.createStatement()) {
            statement.executeUpdate("DELETE FROM user_info;");
            System.out.println("All users are deleted ");
        }
    }

    @Override
    public List<User> getAllUsers() throws SQLException {
        List<User> users = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(URL, username, password); PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM user_info;")) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User tempUser = new User();
                tempUser.setId(resultSet.getInt("id"));
                tempUser.setName(resultSet.getString("name"));
                tempUser.setAge(resultSet.getInt("age"));
                tempUser.setMail(resultSet.getString("mail"));
                users.add(tempUser);
            }

            return users;
        }
    }

}
