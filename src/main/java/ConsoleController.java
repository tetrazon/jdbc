import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

public class ConsoleController {
        //private MySQLController mySQLController;
        private SQLContainer sqlContainer;

    public ConsoleController(){
        //mySQLController = new MySQLController();
        sqlContainer = new SQLContainer();
    }


    public void addUser(String name, int age, String mail) throws SQLException{
        User user = new User(name, age, mail);
        sqlContainer.addUser(user);
    }


    public void deleteUserByName(String name) throws SQLException {
        sqlContainer.deleteUserByName(name);
    }

    public void getAllUsers() throws SQLException {
        List<User> users =  sqlContainer.getAllUsers();
        for (User user: users
             ) {
            System.out.println(user.toString());
        }
    }

    public void deleteAllUsers() throws SQLException {
        sqlContainer.deleteAllUsers();
    }

    public void menu() throws IOException, SQLException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        SQLContainer.registerController(MySQLController.class.getName());
        boolean workFlag = true;
        System.out.println("welcome!\n");
        while (workFlag){
            System.out.println(" press 1 to add user, 2 to delete user, 3 to show all users, 4 for delete all users, 5 for exit");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            int menuOption = Integer.parseInt(bufferedReader.readLine());
            switch (menuOption){
                case 1:
                    System.out.println("input name: ");
                    String name = bufferedReader.readLine();
                    System.out.println("input age (int only)");
                    int age = Integer.parseInt(bufferedReader.readLine());
                    System.out.println("input mail: ");
                    String mail = bufferedReader.readLine();
                    addUser(name, age, mail);
                    break;
                case 2:
                    System.out.println("input name which user you want to delete: ");
                    String nameToDelete = bufferedReader.readLine();
                    deleteUserByName(nameToDelete);
                    break;
                case 3:
                    getAllUsers();
                    break;
                case 4:
                    getAllUsers();
                    deleteAllUsers();
                    break;
                case 5:
                    System.out.println("good bye");
                    workFlag = false;
                    break;
                    default:
                        System.out.println("incorrect input");
                        break;

            }
        }

    }
}
