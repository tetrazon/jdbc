import java.sql.SQLException;
import java.util.List;

public interface DBController {

    void addUser(User user) throws SQLException;
    void deleteUserByName(String name) throws SQLException;
    List<User> getAllUsers() throws SQLException;
    void deleteAllUsers() throws SQLException;


}
