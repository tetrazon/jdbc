public class User {

    private int id;
    private int age;
    private String name;
    private String mail;

    public User(){

    }

    public User(String name, int age, String mail) {
        this.age = age;
        this.name = name;
        this.mail = mail;
    }
    public User(int id, int age, String name, String mail) {
        this.id = id;
        this.age = age;
        this.name = name;
        this.mail = mail;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String toString(){
        return "User id" + id + "\n user name: " + name
                + "\n user age: " + age + "\n user mail: " + mail
                + "\n__________________________";
    }
}
