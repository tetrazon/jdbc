import java.sql.*;
//class for testing connection and basic operations
class TestDB{
    public static void test() throws SQLException, ClassNotFoundException {
        String username = "root";
        String password = "root";
        String URL = "jdbc:mysql://localhost:3306/test?verifyServerCertificate=false&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        Class.forName("com.mysql.jdbc.Driver");
        //        MySQLController dbOperator = new MySQLController();
//        dbOperator.addUser("Ed1", 25, "Ed1@mail.ru");
//        dbOperator.addUser("Goga1", 15, "goga1@mail.ru");
//        dbOperator.deleteUserByName("Goga");
        //dbOperator.deleteAllUsers();
        //DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        try (Connection connection = DriverManager.getConnection(URL,username,password);
             Statement statement = connection.createStatement()) {
            statement.executeUpdate("DROP TABLE  users");
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS users (id SMALLINT NOT NULL auto_increment, name VARCHAR (30) NOT NULL , password SMALLINT NOT NULL,PRIMARY KEY (id));");
            statement.executeUpdate("INSERT INTO users (name, password) VALUES ('Bob', 123);");
            statement.executeUpdate("INSERT INTO users (name, password) VALUES ('Ron', 223);");
            String id = "1' or 1 = '1";
//            ResultSet resultSet = statement.executeQuery("SELECT * FROM users WHERE id = '" + id +"';");
//
//            while (resultSet.next()){
//                System.out.println("user name: " + resultSet.getString("name"));
//                System.out.println("user pass: " + resultSet.getString("password"));
//            }
            //String id = "1";
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users WHERE id = ?");
            preparedStatement.setString(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                System.out.println("user name: " + resultSet.getString("name"));
                System.out.println("user pass: " + resultSet.getString("password"));
            }
        }
    }
}